"""Old -outdated- Version, use the Notebook instead"""

import pandas as pd
import numpy as np
import itertools
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

def preprocess(text):
    result = ""
    for word in text.split():
        result += porter.stem(word) + ' '
    return result
porter = PorterStemmer()

def getSamples(array, count):
    return array[np.random.choice(range(len(array)), count, replace=False),:]

df = pd.read_csv('raw_data_as_csv.csv')
data = np.array(df.values)

dataMI = data[data[:,1] == 'MI',:]
dataHI = data[data[:,1] == 'HI',:]
dataSU = data[data[:,1] == 'SU',:]
dataLH = data[data[:,1] == 'LH',:]
sets = [dataMI, dataHI, dataSU, dataLH]
dataW = np.concatenate([getSamples(_,34) for _ in sets])

# How much words from each topic will be displayed
n_top_words = 5

###############################################################
# ### Options for the classifier and for preprocessing #########
###############################################################

# ### General ##################################################

# The datasets that should be examined. In this case every single group and all together
# Recommendation: In the original data there are different number of samples for each class. In dataW there are 34 randomly chosen samples from each class.
datasets = np.array([dataMI, dataHI, dataSU, dataLH, data, dataW])

# The description for each dataset. Must have same shape as datasets
datasetsDesc = np.array(['Machine Intelligence', 'Human Intelligence', 'Survey', 'Legg&Hutter', 'all', 'all with equal number of samples'])

# Whether the Porter Stemmer should be applied
# Recommendation: With True you don't have results like problem and problems but the words are quite strange and results seem to be better with False
applyPorterStemmer = np.array([True, False])


# ### CountVectorizer ##########################################

# Maximum word frequency for the CoutVectorizer
# Recommendation: values between 0.05 and 0.2, using 0.2 you will allready encounter words like "intelligent"
maxDf = np.array([0.05, 0.1])

# Which range the n_grams should consist of. See doc from CountVectorizer for more details
# Recommendation: Good results could be spotted using (1,3) or (1,2) and (1,1)
nGramRange = np.array([(1,3), (1,1), (2,2)])


# ### LatentDirichilet #########################################

# how much components should be found
# Recommendation: small value, but bigger than 1
components = np.array([2])

###############################################################
###############################################################

###############################################################
# ### Processing ###############################################
###############################################################
counter=1
total = len(datasetsDesc) * len(applyPorterStemmer) * len(maxDf) * len(nGramRange) * len(components)
f = open('result.csv', 'w')
f.write('Used Dataset,Applied PorterStemmer,Maximum frequency,Range of nGrams,Count of topics,topics\n')
for (datasetO, description), applyPorterStemmer_, maxDf_, nGramRange_, components_ in itertools.product(zip(datasets, datasetsDesc), applyPorterStemmer, maxDf, nGramRange, components):
    dataset = datasetO.copy()
    # show process
    print('Computing %d/%d...' % (counter, total))
    counter+=1
    
    # applyPorterStemmer
    if applyPorterStemmer_:
        for i, data in enumerate(dataset):
            dataset[i, 2] = preprocess(data[2])

    # CountVectorizer
    count = CountVectorizer(lowercase=True, strip_accents='ascii', analyzer='word', stop_words='english', max_df=maxDf_, ngram_range=nGramRange_)
    X = count.fit_transform(dataset[:, 2])

    # LatenDirichlet
    lda = LatentDirichletAllocation(n_components=components_, random_state=1, learning_method='batch')
    X_topics = lda.fit_transform(X)

    # show
    # parameters
    print('Used dataset:')
    print(description)
    print('Applied PorterStemmer:')
    print(applyPorterStemmer_)
    print('Maximum frequency:')
    print(maxDf_)
    print('Range of nGrams:')
    print(nGramRange_)
    print('Count of topics:')
    print(components_)
    # result
    feature_names = count.get_feature_names()
    topics = ""
    for topic_idx, topic in enumerate(lda.components_):
        topics += 'Topic %d:' % (topic_idx +1)
        topics += ' - '.join([feature_names[i] for i in topic.argsort() [:-n_top_words -1:-1]])
        topics += '   '
    print(topics)
    print('###################################################################')
    f.write(','.join([description, str(applyPorterStemmer_), str(maxDf_), str(nGramRange_), str(components_), topics])+'\n')
print('Done')
f.close()