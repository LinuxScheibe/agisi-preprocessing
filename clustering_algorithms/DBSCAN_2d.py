# coding: utf-8

import pandas as pd
from sklearn.cluster import DBSCAN
import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics

#prepare: read vector data

file = "documents/Legg&Hutter_2d_w2.csv"
data_df = pd.read_csv(file, names = ['word','x1', 'x2']) 
vectors_arr = data_df[data_df.columns[1:]].values

# Compute DBSCAN
db = DBSCAN(eps=0.3).fit(vectors_arr)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

# Plot result

# Black removed and is used for noise instead.
unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Transparent black used for noise.
        col = [0, 0, 0, 0]

    class_member_mask = (labels == k)

    xy = vectors_arr[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k')

    xy = vectors_arr[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k')

plt.title('Estimated number of clusters: %d' % n_clusters_)

#plotsize
fig = plt.gcf()
fig.set_size_inches(25, 15)

#write result to file
save_file = open("documents/legg&hutter_2d_w2_dbscan.txt", "w+")

save_file.write('Estimated number of clusters: %d \n' % n_clusters_)
save_file.write('Estimated number of noise points: %d \n' % n_noise_)
save_file.write("Homogeneity: %0.3f \n" % metrics.homogeneity_score(data_df['word'], labels))
save_file.write("Completeness: %0.3f \n" % metrics.completeness_score(data_df['word'], labels))
save_file.write("V-measure: %0.3f \n" % metrics.v_measure_score(data_df['word'], labels))
save_file.write("Adjusted Rand Index: %0.3f \n"
      % metrics.adjusted_rand_score(data_df['word'], labels))
save_file.write("Adjusted Mutual Information: %0.3f \n"
      % metrics.adjusted_mutual_info_score(data_df['word'], labels))
save_file.write("Silhouette Coefficient: %0.3f \n"
      % metrics.silhouette_score(vectors_arr, labels))
save_file.write("\r\n")

cluster=labels.tolist()

for c in range(n_clusters_):
    save_file.write("Cluster %s: \n" % str(c+1))
    for i in range(len(cluster)):
        if (cluster[i] == c):
            save_file.write(data_df['word'][i] + "\n")
    save_file.write("\r\n")
    
save_file.write("Noise: \n")
for i in range(len(cluster)):
    if(cluster[i] == -1):
        save_file.write(data_df['word'][i] + "\n")
save_file.close()