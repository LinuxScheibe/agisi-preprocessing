# coding: utf-8

import pandas as pd
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage

#prepare: read vector data

file = "documents/Legg&Hutter_3d_w2.csv"
data_df = pd.read_csv(file, names = ['word', 'x1', 'x2', 'x3']) 
vectors_arr = data_df[data_df.columns[1:]].values

#hierarchisches Clustering

# generate the linkage matrix
linkageMatrix = linkage(vectors_arr, 'ward')

# calculate full dendrogram
dendrogram(
    linkageMatrix,
    orientation='left',
    labels=data_df['word'].tolist(),
    leaf_font_size=8.,  # font size for the x axis labels
)

# hide x-Axsis
frame = plt.gca()
frame.axes.get_xaxis().set_visible(False)

#plotsize
fig = plt.gcf()
fig.set_size_inches(20, 75)

plt.show()