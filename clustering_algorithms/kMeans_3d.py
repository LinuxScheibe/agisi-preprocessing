# coding: utf-8

import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

#prepare: read vector data, set a number of clusters

file = "documents/Legg&Hutter_3d_w2.csv"
data_df = pd.read_csv(file, names = ['word', 'x1', 'x2', 'x3']) 
vectors_arr = data_df[data_df.columns[1:]].values
num_cluster = 6

# cluster
model = KMeans(n_clusters=num_cluster, random_state=42)
model.fit_predict(vectors_arr)

#3D plot
fig = plt.figure(figsize=(20, 15))
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)

ax.scatter(vectors_arr[:, 2], vectors_arr[:, 0], vectors_arr[:, 1],
               c=model.labels_.astype(np.float), edgecolor='k')

#labels behave weird
#for word, i in zip(data_df['word'], range(len(vectors_arr))):
#    ax.text(vectors_arr[i,0],vectors_arr[i,1],vectors_arr[i,2],  '%s' % (word)) 

#write cluster to .txt
cluster = model.labels_.tolist()

save_file = open("documents/Leg&Hutter_w2_3d.txt", "w+")

for c in range(num_cluster):
    save_file.write("Cluster %s: \n" % str(c+1))
    for i in range(len(cluster)):
        if (cluster[i] == c):
            save_file.write(data_df['word'][i] + "\n")
    save_file.write("\n")
save_file.close()