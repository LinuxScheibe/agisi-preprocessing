# coding: utf-8

import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

#prepare: read vector data, set a number of clusters
file = "documents/Legg&Hutter_2d_w2.csv"
data_df = pd.read_csv(file, names = ['word', 'x1', 'x2']) 
vectors_arr = data_df[data_df.columns[1:]].values
num_cluster = 6

model = KMeans(n_clusters=num_cluster, random_state=42)
model.fit_predict(vectors_arr)

#2D plot
fig, ax = plt.subplots()

#labels might behave weird
for word, x1, x2 in zip(data_df['word'], data_df['x1'], data_df['x2']):
    ax.annotate(word, (x1, x2))   
    
plt.scatter(vectors_arr[:, 0], vectors_arr[:, 1], c=model.labels_)

#plotsize
fig = plt.gcf()
fig.set_size_inches(25, 15)

#print cluster in .txt
cluster = model.labels_.tolist()

save_file = open("documents/Leg&Hutter_w2_2d.txt", "w+")

for c in range(num_cluster):
    save_file.write("Cluster %s: \n" % str(c+1))
    for i in range(len(cluster)):
        if (cluster[i] == c):
            save_file.write(data_df['word'][i] + "\n")
    save_file.write("\n")
save_file.close()