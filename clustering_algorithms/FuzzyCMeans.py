# coding: utf-8

import pandas as pd
import numpy as np
import random
import math

def fcm(data_arr, num_cluster, m, e, writeToFile, filePath):
    new_U, old_U, V = init(num_cluster, data_arr, m)
    
    while np.max(new_U - old_U) > e:
        for c in range(num_cluster):
            V[c] = getUpdatedCentroid(new_U, c, len(data_arr), m)
        old_U = new_U
        for c in range(num_cluster):
            for d in range(len(data_arr)):
                new_U[d][c] = getUpdatedMembership(V, V[c], data_arr[d], m)
                
    if writeToFile:
        writeToFile(new_U, filePath)
    
    return display(new_U)

def init(num_cluster, data_arr, fuzzier):
    V = getStartCentroids(num_cluster, data_arr);
    
    amount = num_cluster*len(data_arr)
    old_U = [0]*amount
    old_U = np.reshape(old_U, (len(data_arr), num_cluster))
    new_U = [None]*amount
    new_U = np.reshape(new_U, (len(data_arr), num_cluster))
    
    for c in range(num_cluster):
        for d in range(len(data_arr)):
            new_U[d][c] = getUpdatedMembership(V, V[c], data_arr[d], fuzzier)
    return new_U, old_U, V

#find start centroids based on kmeans++ algorithm
def getStartCentroids(num_cluster, data_arr):
    result = [None]*num_cluster
    ran = random.randint(0, len(data_arr))
    result[0] = data_arr[ran]
    count = 1
    
    while count < num_cluster:
        distances = [None]*len(data_arr)
        for i in range(len(data_arr)):
            distances[i] = getDistanceToNearestCentroid(data_arr[i], result, count)
        result[count] = data_arr[distances.index(np.max(distances))]
        count = count + 1
    return result

def getDistanceToNearestCentroid(point, centroids, iteration):
    result = [None]*iteration
    for i in range(iteration):
        result[i] = getDistance(point, centroids[i])
    return np.min(result)

#calculate Euclidian distance
def getDistance(data, centroid):
    A = data - centroid
    result = math.sqrt(np.dot(A.transpose(), A))
    return result

def getUpdatedMembership(V, centroid, data, fuzzier):
    denominator = 0
    distanceA = getDistance(centroid, data)
    for c in range(num_cluster):
        distanceB = getDistance(V[c], data)
        if distanceB != 0:
            denominator = denominator + (distanceA/distanceB)**(2/(fuzzier-1))
       
    if denominator == 0:
        return 0
    return 1/denominator

def getUpdatedCentroid(U, clusterNum, num_data, fuzzier):
    numerator = 0
    denominator = 0
    for i in range(num_data):
        numerator = numerator +(getMembership(U, clusterNum, i)**fuzzier)*i
        denominator = denominator + getMembership(U, clusterNum, i)**fuzzier
    return numerator/denominator

def getMembership(U, numCluster, numItem):
    return U[numItem, numCluster]

def writeToFile(U, filePath):
    clusters = len(U[0])
    columnLabels = [None]*clusters
    for i in range(clusters):
        columnLabels[i] = 'Cluster ' + str(i+1)
    result = pd.DataFrame.from_records(U, index=data_df['word'], columns=columnLabels)
    
    result.to_csv(filePath, sep='\t', encoding='utf-8')

def display(U):
    clusters = len(U[0])
    columnLabels = [None]*clusters
    for i in range(clusters):
        columnLabels[i] = 'Cluster ' + str(i+1)
    result = pd.DataFrame.from_records(U, index=data_df['word'], columns=columnLabels)
    
    return result

#prepare: read vector data, set number of clusters, fuzzier, termination criterion
file = "documents/Legg&Hutter_2d_w2.csv"
data_df = pd.read_csv(file, names = ['word', 'x1', 'x2']) 
vectors_arr = data_df[data_df.columns[1:]].values

saveFile = 'documents/Leg&Hutter_w2_2d_fuzzy.csv'
num_cluster = 10
fuzzier = 2 # Wert zwischen 1 und unendlich, Wissenschaft hat ergeben, dass Werte zwischen 1 und 2,5 am Besten sind
e = 0.000001 # Wert zwischen 0 und 1, möglichst gering

fcm(vectors_arr, num_cluster, fuzzier, e, False, None)