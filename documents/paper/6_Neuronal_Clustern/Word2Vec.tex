\label{Word2Vec}
Eine weitere Möglichkeit, Vektoren zu bilden, ist es, neuronale Netze zu verwenden. Eine sehr bekannte Methode hierfür ist Word2Vec, ein unsupervised Algorithmus, welcher 2013 durch Tomas Mikolov, Kai Chen, Greg Corrado und Jeffrey Dean, alles Mitarbeiter von Google Inc., vorgestellt wurde  \citep{Word2Vec2013}.

Die nachfolgenden Ideen und Konzepte beruhen zum einen auf der Forschungsarbeit, sowie auf dem Github Repository von Minsuk Heo \citep{Word2Vec2018}, welches entsprechend der Anforderungen des AGISI-Datensatzes angepasst wurde.

\subsection{Wordencoding} \label{sec:wordencoding}
Zu Beginn werden alle Wörter in Zahlen „übersetzt“, das sogenannte Wordencoding. Hierbei handelt es sich um ein einfaches Abzählen und Durchnummerieren von Wörtern. Jedes vorkommende Wort bekommt eine eigene Nummer, vergleichbar mit einer ID (siehe \autoref{wordencoding}). Kommt ein Wort mehrmals vor, wird dies ignoriert, d.h. das Wort bekommt keine neue oder zweite Nummer. Am Ende ergibt sich somit eine Übersicht über alle aufgetretenen Wörter und deren Anzahl.

\begin{figure}[ht]
  \begin{center}
      \makebox[\textwidth]{\includegraphics[scale=0.75]{wordencoding.PNG}}
    \end{center}
  \quelle{Eigene Darstellung}
  \label{wordencoding}
  \caption{Beispiel eines einfachen Wordencodings}
\end{figure}

Somit entstehen nummerische Werte, die durch den Computer verarbeitet werden können. Um diese Wort-Nummer-Kombination darzustellen, werden alle Zahlen in Vektoren umgewandelt. Für das Beispiel aus \autoref{wordencoding} ergeben sich somit die in \autoref{tab:wordencoding} dargestellten Vektoren.


\begin{table}[h]
    \begin{center}
        \bgroup
            \begin{tabularx}{\textwidth}{ | X | X | X | }
                \hline
                  Wort & Encoding\\
                \hline
                 Thank & [1,0,0]  \\
                \hline 
                 You  & [0,1,0] \\
                \hline 
                 Love  & [0,0,1] \\
                \hline
            \end{tabularx}
        \egroup
    \end{center}
    \quelle{Eigene Darstellung}
    \caption{Beispieldaten für ein Wordencoding}
    \label{tab:wordencoding}
\end{table}

Aus diesem Verfahren ergeben sich jedoch folgende Probleme:
\begin{itemize}
\item Ist die Anzahl der Wörter größer als drei, ist dies nicht mehr in einem Koordinatensystem darstellbar. Dieser Punkt ist jedoch nur für die Visualisierung relevant.
\item Alle Wörter besitzen den gleichen Abstand zueinander und zum Ursprung. Folglich können keine Zusammenhänge erkannt werden.
\item Bei der Umsetzung der Wörter in nummerische Werte/Vektoren erfolgt keine Bestimmung der relativen Position zueinander bzw. wie welche Wörter miteinander interagieren. Hierdurch ergibt sich kein Informationsgewinn.
\end{itemize}

\subsection{Bestimmung der Wortnachbarn}
Um das Problem der relativen Positionen zueinander und die Interaktionen der Wörter untereinander zu beheben, werden als nächstes die Nachbarn betrachtet. Für die Betrachtung wird das in \autoref{wortnachbarn_beispiele} sichtbare Beispiel verwendet:
\begin{figure}[h]
    \begin{center}
        ''King brave man''
        
        ''Queen beautiful woman''
    \end{center}
  \quelle{Eigene Darstellung}
  \caption{Beispielsätze für die Bestimmung der Wortnachbarn}
  \label{wortnachbarn_beispiele}
\end{figure}

Zunächst werden die direkten Nachbarn der einzelnen Wörter bestimmt (window"~size~=~1). Diese gehen nicht über das Ende der Aussage hinaus, d.h. die beiden Sätze haben in diesem Beispiel keine Verbindung. Für die Analyse der Definitionen zu Intelligenz wird diese Vorgehensweise der Nachbarbestimmung auf die Definitionen beschränkt. Somit können Nachbarn über Punkte hinweg gefunden werden, jedoch nicht über unterschiedliche Definitionen. Aus dieser Vorgehensweise ergibt sich das in \autoref{direkte_wortnachbarn_beispiele} dargestellte Ergebnis in Bezug auf das obengenannte Beispiel.
\begin{table}[h]
    \begin{center}
        \bgroup
            \begin{tabularx}{\textwidth}{ | X | X | X | }
                \hline
                  Wort & Wortnachbar\\
                \hline
                 King & brave  \\
                \hline 
                 brave  & King \\
                \hline 
                 brave  & man \\
                \hline
                 man & brave\\
                \hline
                 Queen & beautiul  \\
                \hline 
                 beautiful  & Queen \\
                \hline 
                 beautiful  & woman \\
                \hline 
                 woman  & beautiful \\
                \hline
            \end{tabularx}
        \egroup
    \end{center}
    \quelle{Eigene Darstellung}
    \caption{Beispieldaten für die direkten Wortnachbarn (window-size = 1)}
    \label{direkte_wortnachbarn_beispiele}
\end{table}

\autoref{direkte_wortnachbarn_beispiele} ergibt somit eine Look-Up-Table für die direkten Nachbarn. Um den Informationsgewinn zu erhöhen, wird die Suche nach Nachbarn auf zwei Wörter ausgedehnt, d.h. es zählt nicht nur der direkte Nachbar, sondern auch das Vorgänger- bzw. das Nachfolgewort des direkten Nachbarn (window-size = 2). Hierbei ergibt sich für das obige Beispiel \autoref{nachbarsuche_ws_2_beispiele}.
\begin{table}[h]
    \begin{center}
        \bgroup
            \begin{tabularx}{\textwidth}{ | X | X | X | }
                \hline
                  Wort & Wortnachbar\\
                \hline
                 King & brave  \\
                \hline
                 King & man  \\
                \hline 
                 brave  & King \\
                \hline 
                 brave  & man \\
                \hline
                 man & King  \\
                \hline
                  man & brave\\
                \hline
                 Queen & beautiul  \\
                \hline
                 Queen & woman  \\
                \hline 
                 beautiful  & Queen \\
                \hline 
                 beautiful  & woman \\
                \hline
                 woman & Queen  \\
                \hline 
                 woman  & beautiful \\
                \hline
            \end{tabularx}
        \egroup
    \end{center}
    \quelle{Eigene Darstellung}
    \caption{Beispieldaten für die Nachbarsuche bei windows-size = 2}
    \label{nachbarsuche_ws_2_beispiele}
\end{table}

Auch diese ergibt eine Look-Up-Table. Hier ist die Informationsdichte im Vergleich zu \autoref{direkte_wortnachbarn_beispiele} höher, da mehr Verbindungen betrachtet werden. Diese Methode würde sich auf einen beliebig großen Rahmen ausdehnen lassen, sodass beliebig viele Nachbarn vor und nach dem Wort ausgewertet werden können. Wird diese Zahl jedoch zu groß gewählt, kann der Informationsgehalt wieder geringer werden, da dann alle Wörter miteinander direkt in Verbindung gebracht werden würden. Somit ist dieser Punkt bei der späteren Implementierung ein wichtiger Faktor.

\subsection{Wordencoding für Wortnachbarn}
Ist die Phase der Nachbarsuche abgeschlossen, werden alle vorhandenen Wörter wieder encoded. Hierbei bekommt jedes Wort wie zuvor in \autoref{sec:wordencoding} einen individuellen nummerischen Wert. Die Dimension dieser Vektoren ist abhängig von der Anzahl der vorhandenen Wörter. Aus dem obigen Beispiel ergibt sich somit \autoref{tab:Wordencoding_nachbarsuche_ws_2} für die zwei nächsten Nachbarn.
\begin{table}[h]
    \begin{center}
        \bgroup
            \begin{tabularx}{\textwidth}{ | X | X | X | X | }
                \hline
                 Wort & Encoding & Wortnachbar & Encoding\\
                \hline
                 King &	[1,0,0,0,0,0] &	Brave &	[0,1,0,0,0,0] \\
                \hline
                 King &	[1,0,0,0,0,0] &	Man & [0,0,1,0,0,0] \\
                \hline
                 Brave & [0,1,0,0,0,0] & King & [1,0,0,0,0,0] \\
                \hline
                 Brave & [0,1,0,0,0,0] & Man & [0,0,1,0,0,0] \\
                \hline
                 Man & [0,0,1,0,0,0] & King & [1,0,0,0,0,0] \\
                \hline
                 Man & [0,0,1,0,0,0] & Brave & [0,1,0,0,0,0] \\
                \hline
                 Queen & [0,0,0,1,0,0] & Beautiful & [0,0,0,0,1,0] \\
                \hline
                 Queen & [0,0,0,1,0,0] & Woman & [0,0,0,0,0,1] \\
                \hline
                 Beautiful & [0,0,0,0,1,0] & Queen & [0,0,0,1,0,0] \\
                \hline
                 Beautiful & [0,0,0,0,1,0] & Woman & [0,0,0,0,0,1] \\
                \hline
                 Woman & [0,0,0,0,0,1] & Queen & [0,0,0,1,0,0] \\
                \hline
                 Woman & [0,0,0,0,0,1] & Beautiful & [0,0,0,0,1,0] \\
                \hline
            \end{tabularx}
        \egroup
    \end{center}
    \quelle{Eigene Darstellung}
    \caption{Wordencoding der Nachbarsuche bei window-size = 2}
    \label{tab:Wordencoding_nachbarsuche_ws_2}
\end{table}

Für die Darstellung in einem Koordinatensystem sind diese Werte jedoch noch nicht geeignet, da sie in diesem Beispiel sechs Dimensionen aufweisen, was ein kartesisches Koordinatensystem nicht darstellen kann.

\subsection{Umwandlung des Wordencodings in Vektoren} \label{sec:Umwandlung_Wordencoding_Vektoren}
Um die gegebenen Vektoren des Wordencodings aus dem Beispiel der \autoref{tab:Wordencoding_nachbarsuche_ws_2} in zwei- bzw. dreidimensionale Vektoren zu verwandeln, welche in einem kartesischen Koordinatensystem dargestellt werden können, wird ein neuronales Netz verwendet. Das neuronale Netz dient hierbei als Umwandlung und Look-Up-Table zugleich.

\subsubsection{Neuronales Netz }
Ein künstliches neuronales Netz beschreibt in der Informatik ein Verfahren aus der Künstlichen Intelligenz, welches mit Hilfe von künstlichen Neuronen versucht, die Funktionsweise des menschlichen Gehirns nachzubilden. Das neuronale Netz besteht aus einer Eingangsschicht (Input Layer), einer oder mehreren versteckten Schichten (Hidden Layer) und einer Ausgangsschicht (Output Layer). Jede Schicht besteht aus einer gewissen, jedoch frei wählbaren Anzahl an Knoten, die sogenannten Neuronen. Jedes Neuron des Input Layers ist mit jedem Neuron des Hidden Layers und jedes Neuron des Hidden Layers mit jedem Neuron des Output Layers verbunden. Gibt es mehr als einen Hidden Layer, sind die Neuronen innerhalb der Hidden Layer ebenfalls untereinander verbunden. Jede Verbindung hat ihr eigenes Gewicht, welches mit dem Wert des vorherigen Neurons multipliziert wird. Alle in den Neuronen ankommenden, gewichteten Werte werden aufaddiert und an die nächsten Neuronen weitergeleitet. Im Output Layer wird der maximale Wert als Ergebnis ausgewählt \citep{Rashid2017}.

Um das Ergebnis zu verbessern und die Trefferquote des neuronalen Netzes zu erhöhen, muss dieses trainiert werden und die Gewichte innerhalb des Netzes angepasst werden. Für das Training werden ein Input und der dazugehörige Output angegeben. Das Netz berechnet mit Hilfe seiner bisherigen Gewichte das Ergebnis aus dem Input, vergleicht dieses mit dem gewünschten Ergebnis und passt eventuell mittels Backpropagation die Gewichte in dem Netz an. Diese Schritte werden mehrmals mit unterschiedlichen Werten durchgeführt, bis die Trefferquote einen akzeptablen Bereich erreicht hat \citep{Rashid2017}.

\subsubsection{Anwendung auf Beispiel}
Für das Beispiel aus \autoref{tab:Wordencoding_nachbarsuche_ws_2} wird das neuronale Netz folgender Maßen aufgebaut (siehe \autoref{test}):
\begin{itemize}
\item  Als Neuronen für den Input Layer werden die Vektoren aus Tabelle \autoref{tab:Wordencoding_nachbarsuche_ws_2} genutzt. Jede Dimension ergibt ein eigenes Neuron, d.h. die Anzahl der Eingangsneuronen entspricht der Anzahl der Dimensionen. Die Eingangswerte sind 1 oder 0, entsprechend der Vektoren
\item  Der Hidden Layer verarbeitet die Eingangswerte. Diese Vektoren sind die Grundlage der Darstellung der Wörter in einem kartesischen Koordinatensystem und sind entweder zwei- oder dreidimensional aufgebaut.
\item  Der Output Layer ist ähnlich dem Input Layer gestaltet. Die Anzahl der Neuronen entspricht der Anzahl der Dimensionen aus \autoref{tab:Wordencoding_nachbarsuche_ws_2}. Die Ausgabewerte können auf Grund der Berechnungen im Hidden Layer zwischen 0 und 1 angesiedelt sein. Die Werte werden anschließend gerundet, sodass die Werte entweder wieder 0 oder 1 betragen und ein klares Ergebnis zu Stande kommt.
\end{itemize}
\begin{figure}[ht]
  \begin{center}
      \makebox[\textwidth]{\includegraphics[scale=0.6]{Beispiel_Netz.PNG}}
    \end{center}
  \quelle{Eigene Darstellung}
  \caption{Aufbau des neuronalen Netzes}
  \label{test}
\end{figure}

Die für die Darstellung interessanten Daten werden aus dem Hidden Layer übernommen. Diese werden durch mehrmaliges Wiederholen und Ausführen des Trainings verbessert. Für das Training werden die zuvor gewonnen Vektoren aus \autoref{tab:Wordencoding_nachbarsuche_ws_2} als Ein- und Ausgabe verwendet. Dass hierbei immer wieder die gleichen Daten verwendet werden, was zu einem sogenannten Overfitting (Überanpassung) der Gewichte führt, ist in diesem Fall kein Problem, da mit dem gleichen neuronalen Netz keine weiteren Berechnungen oder neue Wortgruppen geprüft und berechnet werden. Ein mögliches Overfitting ist hier sogar von Vorteil, da die gewonnen Vektoren perfekt auf den Datensatz passen und somit sich die Vektoren perfekt einspielen können, wodurch die Darstellung im Koordinatensystem und das Clustering genauer werden. 

\subsection{Anwendung im AGISI-Datensatz}
Das Vorgehen aus \autoref{sec:wordencoding} bis \autoref{sec:Umwandlung_Wordencoding_Vektoren} wird auf den AGISI-Datensatz angewendet, um somit die Definitionen in Vektoren umzuwandeln, graphisch darzustellen und anschließend zu clustern. Für die Umwandlung der Worte in Vektoren wurde Python und verschiedene Bibliotheken verwendet. Die Idee der Umsetzung basiert auf dem öffentlich zugänglichen Github Repository von Minsuk Heo \citep{Word2Vec2018}. 

Um die vorverarbeiteten Daten inklusive der Nachbarsuche zu verwenden und eine Look-Up-Table zu erstellen, woraus anschließend die Vektoren für die graphische Darstellung sowie das Clustering hervorgehen, wird ein neuronales Netz verwendet. Dies wird durch die Pythonbibliothek Tensorflow erstellt. Dies ist eine einfache und schnelle Möglichkeit, ein entsprechendes neuronales Netz aufzuspannen. 
Das neuronale Netz wird mit den gegebenen Wortverbindungen trainiert. Um die Genauigkeit zu erhöhen und bessere Ergebnisse zu erzielen, wird das Training mehrmals durchgeführt, wodurch die Vektoren im Hidden Layer verbessert werden und somit das Ergebnis besser wird. Ein Maß für die Genauigkeit ist der Loss. Hierbei wird angegeben, wie oft das tatsächliche Ergebnis von dem gewünschten Ergebnis abweicht, d.h. wie oft es vorkommt, das ein Wort in den Input Layer gegeben wird und im Output Layer das falsche Ergebnis erhält. Der Loss kann theoretisch einen Wert zwischen 0\% und 100\% annehmen. Je näher dieser Wert an 0 kommt, desto besser ist das Netz trainiert und desto genauer funktioniert es.
Für eine hohe Genauigkeit ist es wichtig, eine angemessene Anzahl an Iterationen für das Training zu wählen. Ist dies nicht der Fall, können sich folgende Probleme ergeben:
\begin{itemize}
\item  Ist die Anzahl der Iterationen zu klein, ist die Genauigkeit des neuronalen Netzes zu schlecht und das Ergebnis könnte verfälscht werden.
\item 	Ist die Anzahl der Iterationen zu groß, dauert das Training zu lange und der Aufwand wäre bezüglich der Genauigkeit nicht zu vertreten.
\end{itemize}
Für das Trainieren des neuronalen Netzes wurden 25000 Iterationen gewählt. Dies konnte mit einem vertretbaren Aufwand berechnet werden. Eine größere Anzahl an Iterationen, z.~B. 50000 Iterationen, wäre ebenfalls in einem zeitlich noch annehmbaren Rahmen gewesen, jedoch hat sich die Genauigkeit hierbei nicht mehr signifikant geändert. Der Wert des Loss sinkt zwar weiter gegen Null, jedoch sind diese Schritte nur sehr kleinschrittig und kaum bemerkbar. 

\begin{figure}[h!]
  \begin{center}
      \makebox[\textwidth]{\includegraphics[scale=0.7]{Grafik_Loss.PNG}}
    \end{center}
  \quelle{Eigene Darstellung}
  \caption{Entwicklung des Loss im neuronalen Netz}
  \label{Loss}
\end{figure}

Wie in \autoref{Loss} zu sehen, erinnert die Kurve des Loss an ein begrenztes Wachstum bzw. in diesem Fall an einen begrenzten Zerfall. Das heißt, das Ergebnis nähert sich immer weiter einer bestimmten Grenze, unter die es niemals fallen wird, egal wie lange der Prozess fortgesetzt wird. Der weitere Zerfall wird jedoch immer kleiner und ab einer bestimmten Grenze ist die Rate zu klein um einen wichtigen Unterschied zu machen.

In dem Training für den AGISI-Datensatz ist diese Grenze bereits nach 25000 Iterationen zu bemerken. Der Loss ändert sich nur noch geringfügig, auf das neuronale Netz und die Vektoren haben weitere Iterationen kaum noch Einfluss.

Ist das neuronale Netz trainiert, können die Vektoren aus dem Hidden Layer bezogen werden. Diese stellen in Verbindung mit dem codierten Wort die Vektoren für die Darstellung im kartesischen Koordinatensystem, sowie das Clustering dar.