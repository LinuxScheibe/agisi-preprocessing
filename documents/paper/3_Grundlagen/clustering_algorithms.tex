Clusterverfahren dienen der Klassifikation von Daten. Die einzelnen Klassen sind vorher meist unbekannt, wodurch es notwendig ist, diese aus den vorhandenen Daten zu konstruieren. Ziel ist es also, Muster anhand von Stichproben zu erkennen.

Eine Stichprobe ist eine Ansammlung von Beobachtungen. Es wird vorausgesetzt, dass eine Stichrobe eine Gruppenstruktur ausweist. Durch diese Struktur ist es möglich die Stichprobe in Gruppen aufzuteilen. Diese Gruppen werden auch Cluster genannt. Innerhalb eines Clusters sollten die Daten möglichst homogen sein d.~h., dass sie eine große Ähnlichkeit zueinander aufweisen. Im Gegenzug dazu sollten Daten in unterschiedlichen Clustern sich unterscheiden. Im Idealfall lässt sich eine Stichprobe in homogene, klar unterscheidbare Cluster aufteilen.

Da es kein formales Modell zur Erstellung von Klassifikationen gibt, existieren etliche Clusterverfahren. Der Erfolg des Clusterverfahrens hängt meist von den Daten und dem verwendeten Verfahren ab. Letztendlich entscheidet der Anwender der Verfahren ob die Resultate sinnvoll sind \citep{clusteranalyse2010}. 
Im Nachfolgenden wird auf vier ausgesuchte Verfahren eingegangen, mit denen in dieser Arbeit versucht wird, Cluster zu finden.

\newpage

\subsection{K-Means Algorithmus}\label{subsec:kMeans}
K-Means ist ein exklusives Clusterverfahren. Dies bedeutet, dass ein Datum nur einem Cluster angehören kann. Die folgenden Gleichungen sind aus \citep{clusteranalyse2010} entnommen. Der Algorithmus ordnet \(N\) Daten in einem \(I\)-dimensionalem Raum \(k\) Clustern zu. Jedes Cluster hat als Parameter den Vector \(m^{(k)}\), der mean genannt wird und das Zentrum eines Clusters beschreibt. Ein Datum wird als \(\{x^{(n)}\}\) notiert. Jeder Vector \(x\) hat \(I\) Komponenten \(x_i\). Nehmen wir an, dass \(x\) in einer realen Umgebung existiert, kann der Abstand beispielsweise wie folgt bestimmt werden:
\begin{equation}
d(x, y) = \frac{1}{2}\sum \limits_{i=1}^{I}(x_i - y_i)^2.
\end{equation}
Der Abstand soll minimal sein. K-Means entspricht daher mathematisch der Optimierung der Funktion
\begin{equation}
J = \sum \limits_{i=1}^{k}\sum \limits_{x_j\in S_i}\Vert x_j - m_i\Vert^2,
\end{equation}
wobei die Menge \(S_i\) die Menge aller Datenpunkte innerhalb eines Clusters ist.

Zu Beginn des Algorithmus muss \(m(k)\) für jedes Cluster initialisiert werden. Dies kann zum Beispiel mit zufällig ausgewählten Daten aus der Menge \(N\) erfolgen. Danach werden zwei Schritte solange wiederholt, bis das Optimum erreicht ist. Der erste Schritt ist der Zuordnungsschritt. Jedes Datum n wird dem dichtesten \(m(k)\), gemäß der \autoref{equ:kMeansZuordnung} zugeordnet, wobei \(t\) den Iterationsschritt abbildet.
\begin{equation}\label{equ:kMeansZuordnung}
S^{(t)}_i = \{ x_j : \Vert x_j - m^{(t)}_i\Vert^2 \leq \Vert x_j - m^{(t)}_{i^*}\Vert^2\text{ für alle }i^* = 1, \dots, k\}
\end{equation}
Danach folgt der Aktualisierungsschritt. Das Clusterzentrum wird mit Hilfe der \autoref{equ:kMeansUpdate} neu berechnet, sodass dieses dem Mittelwert der Abstände der zugeordneten Daten entspricht.
\begin{equation}\label{equ:kMeansUpdate}
m^{(t+1)}_i = \frac{1}{\vert S^{(t)}_i\vert}\sum \limits_{x_j\in S^{(t)}_i}x_j
\end{equation}
Der Algorithmus terminiert immer mit einer Lösung. Diese Lösung muss jedoch nicht die optimale Lösung sein, da der Algorithmus stark von seinen Startparametern abhängig ist. Mehrere Durchläufe mit verschiedenen Startparametern sind daher ratsam \citep{davidmackay2003}. 

Wie an den Berechnungen erkennbar ist, wird \(k\) vorausgesetzt. Der Anwender muss daher \(k\) zu Beginn festlegen. Sollte die Anzahl der benötigten Cluster nicht bekannt sein, kann man das Verfahren mehrfach mit variierender Clusteranzahl durchgeführt werden. Dabei wird mit der kleinstmöglichen Clusteranzahl begonnen und mit der maximalen Anzahl beendet. Für jedes Resultat wird das Optimalitätskriterium überprüft, um die am besten geeignete Clusteranzahl zu finden. Bei der Überprüfung wird nach einer Sprungstelle gesucht, da mit der wachsenden Anzahl von \(k\) das Optimalitätskriterium sich dem Optimum von 0 nähert. Es kann jedoch auch vorab ein anderes Clusterverfahren angewendet werden von dessen Resultat auf die Startparameter geschlossen werden kann. Für k-Means eignet sich dafür das Ward'sche Verfahren (\autoref{subsec:hierarchical}) besonders gut, da die Optimalitätskriterien übereinstimmen \citep{clusteranalyse2010}.

Für diesen Algorithmus existieren verschiedene Variationen. K-Median verwendet statt der Euklidischen Distanz die Manhattan Distanz. Dabei werden die oben beschrieben Schritte verfolgt. Der einzige Unterschied ist, dass nicht der Mittelwert, sondern der Median berechnet wird zum Aktualisieren der Clusterzentren \citep{kmedian1997}. K-Means++ wählt die initialen Clusterzentren nicht zufällig aus. Das erste Clusterzentrum ist zufällig gewählt, alle weiteren Zentren sind möglichst weit von den bereits gewählten Zentren entfernt. Die Resultate sind so gut wie die vom klassischen k-Means Algorithmus, k-Means++ terminiert jedoch schneller \citep{kmeans++2004}.

\subsection{Fuzzy C-Means Algorithmus}
Fuzzy C-Means ist ein überlappendes Clusterverfahren. Dies bedeutet, dass jedes Datum mit einer bestimmten Wahrscheinlichkeit einem Cluster angehört. Prinzipiell ist es also möglich, dass ein Datum mehreren Clustern zugeordnet ist. Nachfolgende Gleichungen sind entnommen aus \citep{fcm1984}.

Der Algorithmus teilt \(N\) Daten in einem \(I\)-dimensionalem Raum \(c\) Clustern mit einem Zugehörigkeitsgrad \(u_{ik}\) zu. \(u_{ik}\) befindet sich im Intervall \([0; 1]\). Je höher der Zugehörigkeitsgrad umso stärker ist die Zugehörigkeit zu einem Cluster. Der Algorithmus verwendet die folgende Zielfunktion:
\begin{equation}
    J(U,V)=\sum \limits_{i=1}^{c} \sum \limits_{k=1}^{N}u^m_{ik}d_{ik}^2.
\end{equation}
Die Partitionsmatrix \(U\) ist eine Matrix der Form \(c \times N\) und gibt den Zugehörigkeitsgrad \(u_{ik}\) wieder. Die Matrix \(V\) gibt die Vektoren \(v_i\) der einzelnen Clusterzentren wieder. Der Fuzzier \(m\) bestimmt, wie streng die Daten einem Cluster zugeordnet werden. Für \(m\) gibt es keinen optimalen Wert. \(m=1\) führt dazu, dass die Cluster nicht überlappend sind d.~h., dass der Zugehörigkeitsgrad entweder 0 oder 1 ist. Damit erhält man ähnliche Resultate wie beim k-Means Algorithmus. Je höher \(m\) gewählt wird desto mehr verschwimmen die Cluster ineinander. Für die meisten Daten erhält man für \(1.5 \leq m \leq 3.0\) gute Resultate \citep{fcm1984}. Die Distanz \(d_{ik}^2\) wird mit der \autoref{equ:fcmDistanz} berechnet.
\begin{equation}\label{equ:fcmDistanz}
    d_{ik}^2 = (x_k - v_i)^T(x_k - v_i)
\end{equation}
\newpage
Die Werte \(u_{ik}\) und \(v_i\) werden durch die Minimierung der Zielfunktion bestimmt. Die Daten werden den Clustern so zugeordnet, dass die Summe der quadrierten Distanzen minimal ist. Um das Minimierungsproblem zu lösen, werden die folgenden Nebenbedingungen gestellt:
\begin{enumerate}
	\item Für alle Daten \(x_k\) gilt \(\sum \limits_{i=1}^{c} u_{ik} = 1\)
	\item Für alle Cluster \(c_i\) gilt \(\sum \limits_{k=1}^{N} u_{ik} > 0\)
\end{enumerate}
Löst man das Minimierungsproblem entsprechend erhält man
\begin{equation}\label{equ:fcmUpdateV}
    v_i = \frac{\sum \limits_{k=1}^{N}u^m_{ik}x_k}{\sum \limits_{k=1}^{N}u^m_{ik}}
\end{equation}
und
\begin{equation}\label{equ:fcmUpdateU}
    u_{ik}=\frac{1}{\sum\limits_{j=1}^{C}(\frac{d_{ik}}{{d_{jk}}})^{(\frac{2}{m-1})}}.
\end{equation}
Zu Beginn des Algorithmus müssen \(c\), \(m\) und \(V\) gewählt und die Startpartitionsmatrix \(U^{(0)}\) initialisiert werden. Danach werden zwei Schritte solange wiederholt, bis der Schwellwert \(\varepsilon\) unterschritten wird. Im ersten Schritt werden alle \(v_i\) nach \autoref{equ:fcmUpdateV} neu berechnet und \(V\) dementsprechend aktualisiert. Im zweiten Schritt werden dann die Zugehörigkeiten mit \autoref{equ:fcmUpdateU} neu berechnet und \(U\) wird aktualisiert. Das Terminationskriterium lautet wie folgt:
\begin{equation}
    \Vert U^{(r)} - U^{(r-1)}\Vert < \varepsilon.
\end{equation}
\(r\) bildet den Iterationsschritt ab. \(\varepsilon\) ist ein Schwellwert und sollte möglichst gering sein. Durch das Terminationskriterium wird sichergestellt, dass der Algorithmus terminiert. Daher erhält man mit diesem Algorithmus immer eine Lösung. Diese muss jedoch nicht die optimale sein, da der Algorithmus stark von seinen Startparametern abhängig ist. Mehrere Durchläufe mit verschiedenen Startparametern sind daher ratsam \citep{fcm1984}.

Wie auch bei k-Means wird die Anzahl der Cluster vorrausgesetzt. Da durch ein entsprechend gewähltes \(m\) \((m=1)\) , wie oben erklärt, k-Means als Spezialform von Fuzzy C-Means gesehen werden kann, können die selben Methoden zum Ermitteln der Clusteranzahl (siehe \autoref{subsec:kMeans}), falls diese nicht bekannt sein sollte, angewandt werden.

\newpage

\subsection{Hierarchische Clusterverfahren}\label{subsec:hierarchical}
Bei hierarchischen Clusterverfahren wird eine Hierarchie der Daten aufgebaut. Der Algorithmus beginnt dabei mit den kleinstmöglichen Gruppen, die genau ein Datum enthalten. Dann werden die Gruppen, die sich nach Abstandsmaß am ähnlichsten sind, vereint. Dies wird solange wiederholt, bis alle Daten wieder einer Gruppe angehören. Mit jeder Iteration \(i\) muss die Distanz gemäß dem gewählten Abstandsmaß der einzelnen Gruppen neu berechnet werden, bevor Gruppierungen vereint werden. Es gibt mehrere Möglichkeiten, das erwähnte Abstandsmaß zu wählen. Im Folgenden wird ein kleiner Einblick auf einzelne Möglichkeiten gegeben. Danach wird ausführlich auf das Ward’sche Verfahren eingegangen, da dieses für die vorliegende Arbeit gewählt wurde.

\begin{description}
    \item[\bfseries Single Linkage:] Bei diesem Abstandsmaß wird der minimale Abstand verwendet, d.~h. der minimale Abstand zweier Daten einer Gruppe entspricht dem Abstand der Gruppen.
    \item[\bfseries Complete Linkage:] Bei diesem Abstandsmaß wird der maximale Abstand verwendet, d.~h., dass der maximale Abstand zweier Daten einer Gruppe dem Abstand der jeweiligen Gruppen entspricht.
    \item[\bfseries Average Linkage:] Der Abstand zweier Gruppen wird als Mittelwert der Abstände zwischen allen Datenpaaren definiert.
\end{description}

Das Ward’sche Verfahren legt fest, dass die Binnenvarianz der Gruppierungen nur minimal wächst. Dadurch wird die Heterogenität berücksichtigt, weshalb dieses Verfahren häufig bevorzugt wird. Es werden konvexe Cluster gebildet und eine gleichmäßige Verteilung der Daten auf die verschiedenen Cluster begünstigt \citep{Wiedenbeck2001}. Alle in diesem Abschnitt erwähnten Formeln wurden aus \citep{ward1963} übernommen. Mathematisch betrachtet sieht der anfangs beschriebene Algorithmus mit Wahl des Ward'schen Verfahrens wie folgt aus:

Ausgegangen wird von der Zielfunktion
\begin{equation}
    ESS(M)=\sum \limits_{i=1}^{n}x_i^2-\frac{1}{n}(\sum \limits_{i=1}^{n}x_i)^2.
\end{equation}
Die Zielfunktion beschreibt das Anstreben einer Gruppenvereinigung, wobei \(x_i\) das \(i\)-te Element einer Menge \(M\) ist. Je niedriger der Wert der Zielfunktion umso erstrebenswerter ist eine Vereinigung. 

Begonnen wird mit der Menge \(U\), welche aus \(n\) Teilmengen besteht. \(n\) entspricht der Gesamtanzahl der Daten.  Um aus \(n-1\) Teilmengen zu reduzieren, muss eine neue Teilmenge aus den ursprünglichen \(n\) Teilmengen gebildet werden, beispielsweise
\begin{equation}
    [S(1,n)] \cup [S(2,n)]=\{e_1,e_2\}.
\end{equation}
Dabei ist zu beachten, dass die Veränderung des Wertes der Zielfunktion minimal gehalten wird.
Die Zielfunktion muss für alle \(n(n -1)/2\) Möglichkeiten der Vereinigungen evaluiert werden. Eine Teilmenge wird beschrieben als \(S(i,n)\), wobei \(i\) zur Identifizierung der Teilmenge dient und \(n\) die Anzahl der unter Betracht zu ziehenden Teilmengen abbildet. Eine neue Teilmenge wird dementsprechend wie folgt definiert:
\begin{equation}
    S(p_{n-1}, n - 1) = [S(p_{n-1}, n)] \cup [S(q_{n-1}, n)]. 
\end{equation}
\(p_{n-i}\) stellt die kleinere Zahl und \(q_{n-i}\) die größere Zahl zur Identifikation einer Teilmenge dar. Der dazugehörige Wert der Zielfunktion wird in der Form von \autoref{equ:NotationESS} festgehalten.
\begin{equation}\label{equ:NotationESS}
    Z[p_{n-1}, q_{n-1}, n - 1] = ESS([S(p_{n-1}, n - 1)])
\end{equation}
In der nächsten Iteration sind \(n-1\) Teilmengen vorhanden. Dadurch muss das Folgende definiert und betrachtet werden:
\begin{equation}
\begin{split}
    {S(i, n-1) = S(i ,n)} \\
    \text{mit } i = 1, 2, \dots, n;\text{ }i \neq p_{n-1} \text{ und } i \neq q_{n-1}   
\end{split}
\end{equation}
und
\begin{equation}
\begin{split}
    S(p_{n-1}, n -1) = [S(p_{n-1}, n)] \cup [S(q_{n-1}, n)] \\
    \text{mit } i=p_{n-1}.
\end{split}
\end{equation}
Dementsprechend müssen alle \((n-I) (n -2)/2\) möglichen Vereinigungen analog zur Reduzierung der Teilmengen von n auf \(n-1\) evaluiert werden. Die korrekte Notation sieht dann gemäß \autoref{equ:Teilmenge} und \autoref{equ:ESSWert} aus.
\begin{equation}\label{equ:Teilmenge}
    S(p_{n-2}, n - 2) = [S(p_{n-2}, n - 1)] \cup [S(q_{n-2}, n-1)]
\end{equation}
\begin{equation}\label{equ:ESSWert}
\begin{split}
    Z[p_{n-2}, q_{n-2}, n - 2] \\
    \text{ mit } p_{n-2} < q_{n-2}
\end{split}
\end{equation}
Analog dazu werden alle weiteren Iterationsschritte falls notwendig durchgeführt, bis die Menge \(U\) wiederhergestellt wurde. 

Es kann bewiesen werden, dass der Algorithmus immer terminiert. Dadurch, dass der Algorithmus lediglich die Menge der Daten als Startparameter benötigt, erhält man dasselbe Resultat für dieselben Daten. Wiederholungen sind daher nicht notwendig \citep{ward1963}.

\subsection{DBSCAN Algorithmus}
DBSCAN ist ein dichtebasiertes Clusterverfahren. Es ermöglicht das Finden willkürlicher Cluster, wobei Rauschpunkte ignoriert und gesondert zurückgegeben werden.
Der Algorithmus definiert das Folgende \citep{dbscan1996}:
\begin{description}
    \item[\bfseries \(\varepsilon\)-neighborhood] Die \(\varepsilon\)-neighborhood ist definiert durch
    \begin{equation}
        N_\varepsilon(p)=\{q \in D | dist(p,q) \leq \varepsilon\}.
    \end{equation}
    \(D\) ist die Menge der Daten, \(\varepsilon\) definiert den erlaubten Maximalabstand zweier Punkte und \(dist(p,q)\) beschreibt den Abstand der Punkte \(p\) und \(q\). Es wird vorrausgesetzt, dass für jeden Punkt \(p\) in einem Cluster \(C\) auch ein Punkt \(q\) in Cluster \(C\) enthalten ist, sodass \(p\) direkt dichte-erreichbar von Punkt \(q\) ist.
    \item[\bfseries Direkt dichte-erreichbar] Ein Punkt \(p\) ist direkt dichte-erreichbar von einem Punkt \(q\), wenn
    \begin{enumerate}
        \item \(p \in N_\varepsilon(q)\) und
        \item \(\vert N_\varepsilon(q)\vert \geq MinPts\).
    \end{enumerate}
    \(MinPts\) ist ein Parameter, der eine Mindestanzahl an Punkten in einer \(\varepsilon\)-neighborhood festlegt.
    \item[\bfseries Dichte-erreichbar] Ein Punkt \(p\) ist dichte-erreichbar von einem Punkt \(q\), wenn eine Folge von Punkten \(p_1, \dots, p_n; p_1 = q; p_n = p\) existiert, sodass \(p_{i+1}\) direkt dichte-erreichbar von \(p_i\) ist.
    \item[\bfseries Dichte-verbunden] Ein Punkt \(p\) ist dichte-verbunden mit einem Punkt \(q\), wenn es einen Punkt \(o\) gibt, von dem aus sowohl \(p\) als auch \(q\) dichte-erreichbar sind.
    \item[\bfseries Cluster] Ein Cluster \(C\) ist eine nicht leere Teilmenge von \(D\) und erfüllt die folgenden Bedingungen:
    \begin{enumerate}
        \item \(\forall p, q: (p \in C \land q \text{ ist dichte-erreichbar von } p) \rightarrow q \in C\)
        \item \(\forall p, q \in C: p \text{ ist dichte-verbunden zu } q\)
    \end{enumerate}
    Cluster \(C\) besteht aus Kernpunkten und Randpunkten. Randpunkte sind jene Punkte am Rand des Clusters \(C\), wohingegen Kernpunkte innerhalb des Clusters liegen.
    \item[\bfseries Rauschen] Als Rauschen werden alle Punkte aus \(D\) bezeichnet, die keinem Cluster \(C_i\) angehören. Mathematisch kann dies wie folgt formuliert werden:
    \begin{equation}
        Rauschen = \{ p \in D \vert \forall i:p \notin C_i\}
    \end{equation}
\end{description}
Um eine Korrektheit des Algorithmus gewährleisten zu können, werden die folgenden zwei Lemmata benötigt.
\begin{description}
    \item[\bfseries Lemma~1] Lass \(p \in D\) und \(\vert N_\varepsilon(p)\vert \geq MinPts\) sein. Dann ist die Menge \newline
    \(O = \{ o \vert o \in D \text{ und } o \text{ ist dichte-erreichbar von } p\}\) ein Cluster.
    \item[\bfseries Lemma~2] Lass \(C\) ein Cluster und \(p\) ein willkürlicher Punkt in \(C\) sein. Für den Punkt \(p\) gilt \(\vert N_\varepsilon(p)\vert \geq MinPts\). Dann entspricht \(C\) der Menge \newline
    \(O = \{ o \vert o \text{ ist dichte-erreichbar von } p\}\).
\end{description}
Um beginnen zu können, werden die in den Definitionen erwähnten Startparameter \(\varepsilon\) und \(MinPts\) benötigt. Es wird ein willkürlicher Punkt \(p\) aus der Menge \(D\) gewählt. Danach werden alle von \(p\) dichte-erreichbaren Punkte ermittelt. Werden dichte-erreichbare Punkte gefunden, ist \(p\) ein Kernpunkt und es wird ein Cluster konstruiert (Lemma~2). Dann wird mit dem nächsten Punkt fortgefahren, solange bis von jedem Punkt in \(D\) die dichte-erreichbaren Punkte ermittelt wurden. Gemäß der Definition eines Clusters für dieses Verfahren können zwei naheliegende Cluster vereinigt werden \citep{dbscan1996}.

Die Resultate des Algorithmus sind von seinen Startparametern \(\varepsilon\) und \(MinPts\) abhängig. Beide können mit einer Heuristik bestimmt werden, die hier nicht weiter beschrieben wird.