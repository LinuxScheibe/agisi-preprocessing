Eine grundlegende Annahme des \ac{tf-idf}-Verfahrens ist, dass sich die Bedeutung eines Textes hauptsächlich aus den darin enthaltenen Wörtern ergibt, deren Position jedoch zu vernachlässigen ist. Dabei spielt neben der bloßen Existenz eines Terms in einem Dokument auch die Gewichtung dieses Terms eine wichtige Rolle. So sollen Terme, die in zu vielen Dokumenten vorkommen geringer gewichtet werden als Terme die eher selten vorkommen \citep[S.~3658]{chen2017}. 

Die nachfolgende Formel zeigt die Berechnung dieses Maßes. Dabei bezeichnet
\begin{math} 
    TF_{t,d}
\end{math}
 die Häufigkeit eines Terms t im Dokument d, 
\begin{math}
    |D|
\end{math}
 die Gesamtzahl aller Dokumente und
\begin{math}
    DF_{t,D}
\end{math}
 die Häufigkeit des Terms t in allen Dokumenten \citep[S.~4]{compStudy}.
Die errechneten Werte haben die Eigenschaft, mit der Häufigkeit eines Terms im Dokument positiv zu korrelieren und mit der Häufigkeit des Terms im gesamten Textkorpus negativ zu korrelieren \citep[S.~3]{compStudy}. Da sich die Definitionen in ihrer Länge deutlich unterscheiden, sollten die Häufigkeiten der Terme in den Dokumenten (
\begin{math} 
    TF_{t,d}
\end{math}
) noch durch die maximale Häufigkeit eines Terms in diesem Dokument geteilt werden (
\begin{math}
    max_{t' \in d} TF_{t',d}
\end{math}
), um diese Werte zu normalisieren und das Ergebnis durch lange Texte nicht zu verzerren \citep[S.~29f.]{Baeza-Yates1999}. Ein Beispiel für einen sehr langen Text mit entsprechend großen Termhäufigkeiten ist in der ersten Spalte in Abbildung \ref{dtm} zu sehen.

    \begin{displaymath}
        TFIDF_{t,d} = \frac{TF_{t,d}}{max_{t' \in d} TF_{t',d}} \cdot\log\frac{|D| + 1}{DF_{t,D} + 1}
    \end{displaymath}

Das Ergebnis ist ein hochdimensionaler Raum, wobei jeder Term eine eigene orthogonale Dimension bildet. In diesem können die einzelnen Dokumente als Vektor dargestellt werden. Aufgrund der erfolgten Gewichtung der Termhäufigkeiten mittels \ac{tf-idf} weisen Vektoren, die seltene aber aussagekräftige Wörter teilen, eine hohe Ähnlichkeit auf \citep[S.~3]{compStudy}. Die erzeugten Vektoren können anschließend den bereits in Kapitel \ref{sec:clusterverfahren} behandelten Clusterverfahren übergeben werden.

Das \ac{tf-idf}-Verfahren bringt neben dem Vorteil der sinnvollen Gewichtung der Terme auch einige Nachteile mit. So ist das Verfahren nicht darauf ausgelegt, n-Gramme zu verarbeiten \citep[S.~3]{compStudy}. Zwar können ohne Anpassungen auch n-Gramme übergeben werden, jedoch führt dies zu einer erneuten Vervielfachung der auch bei einfachen Termen schon vielen Dimensionen, welches in den weiteren Schritten zu hohem Rechenaufwand führt. Weiterhin hat dies auch den sogenannten ''Fluch der Dimensionalität'' zur Folge \citep[S.~4]{compStudy}.

Der Fluch der Dimensionalität beschreibt das Phänomen, dass der Merkmalsraum einer Datenmenge mit zunehmender Dimensionaliät immer dünner besetzt wird. Dies führt dazu, dass selbst vergleichsweise ähnliche Dokumente im Vektrorraum weit voneinander entfernt liegen, was die Ergebnisse der Clusterverfahren negativ beeinflusst. \citep[S.~122]{Raschka2018}. Auch im Anwendungsfall wird dieses Problem sichtbar: Werden bereits bei n=1 (1447 Merkmalsdimensionen, siehe Tabelle \ref{ngramme}) für die meisten Definitionen über 1000 Dimensionen unbesetzt sein, so werden es bei n=2 (6531 Merkmalsdimensionen) über 6000 sein.

Ein weiterer Nachteil ist, dass es nicht möglich ist, inkrementelle Updates durchzuführen, ohne das gesamte Modell neu zu berechnen \citep[S.~4]{compStudy}. Dies kann jedoch in diesem Anwendungsfall vernachlässigt werden, da die Datenmenge sehr klein ist und somit das Modell vergleichsweise schnell neu berechnet werden kann.

Nachfolgend sollen nun die zuvor beschriebenen Clusterverfahren auf die vorhandene Datenbasis angewendet werden.