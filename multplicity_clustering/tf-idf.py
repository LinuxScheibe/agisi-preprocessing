import numpy as np
import math

def tfidf_transformer(documentTermMatrix):
    """Makes a tfidf matrix for given dtm
    Remove header column and line in advance
    pass [i[1:] for i in dtm[1:]]"""
    dtm = np.array(documentTermMatrix).astype(np.float)
    sums = []
    results = []
    for row in dtm:
        sums.append(row.sum())
    maxs = [np.amax([i[j] for i in dtm]) for j in range(len(dtm[0]))]
    for i, row in enumerate(dtm):
        for j, cell in enumerate(row):
            results.append((cell/maxs[j]) * math.log(((len(dtm[0]))+1)/(sums[i]+1)))
    return np.reshape(results, np.shape(dtm))

def importDtm(path):
    return np.load(path)

def exportTfIdf(path, tfidf):
    np.save(path, tfidf)

for n in range(1,3+1):
    dtm = importDtm('dtm_'+str(n)+'.npy')
    dtm = np.array(dtm)

    tfidf = tfidf_transformer([i[1:] for i in dtm[1:]])

    exportTfIdf('tfidf_'+str(n), tfidf)
    exportTfIdf('terms_'+str(n), [i[0] for i in dtm])