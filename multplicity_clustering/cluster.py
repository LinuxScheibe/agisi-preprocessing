"""Clusters and visualizes data using PCA"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.decomposition import LatentDirichletAllocation as LDA
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from scipy.spatial.distance import cosine
import itertools


def importData(path):
    return np.load(path)

def fitTransFormPCA(n, data, standardScale=False):
    """Reduces data to n principal components"""
    if standardScale:
        sc = StandardScaler()
        data = sc.fit_transform(data)
    pca = PCA(n_components=n)
    return pca.fit_transform(data)

def outputDistances(data, metric="euclidean"):
        """Output all distances sorted"""
        dists = []
        for (i, line1), (j, line2) in itertools.product(enumerate(data),enumerate(data)):
                dist = 0
                if (metric=="euclidean"):
                        dist = np.linalg.norm(line1 - line2)
                elif (metric=="cosine"):
                        dist = cosine(line1, line2)
                dists.append(dist)

        print("Median is: "+str(np.median(dists)))
        print("Average is: "+str(np.sum(dists)/len(dists)))
        sort = sorted(dists)
        f = open('distances.txt', 'w+')
        for i in sort:
                f.write(str(i)+'\n')
        f.close()

def visualize(data, labels):
        pca = fitTransFormPCA(2, data, False)
        colors = ['black', 'silver', 'rosybrown', 'red', 'sienna', 'sandybrown', 'tan', 'gold', 'darkkhaki', 'olivedrab', 'chartreuse', 'palegreen', 'mediumspringgreen', 'lightseagreen', 'deepskyblue', 'royalblue', 'navy', 'darkorchid', 'mediumvioletred', 'palevioletred']
        #colors = ['black', 'red', 'navy', 'limegreen', 'gold', 'silver', 'chartreuse', 'palevioletred', 'olivedrab', 'sienna']
        for line, color in zip(pca, labels):
                plt.scatter(line[0], line[1], c=colors[color % len(colors)])
        plt.show()

def loadData(tfidf=True, ngram=1, transposed=True):
        data = []
        if (tfidf):
                data = np.array(importData('tfidf_'+str(ngram)+'.npy'))
        else:
                data = np.array([i[1:] for i in importData('dtm_'+str(ngram)+'.npy')[1:]]).astype(int)
        if (transposed):
                data = data.T

        return data  

def getSampleWords(labels, ngram=1):
        data = np.array(importData('tfidf_'+str(ngram)+'.npy'))
        terms = np.array([i[0] for i in importData('dtm_'+str(ngram)+'.npy')[1:]])
        sums = np.sum(data, axis=1)
        for i in sums:
                print(i)
        result = zip(terms, sums, labels)
        f = open('result.csv', 'w+')
        for a,b,c in result:
                f.write(str(a)+','+str(b)+','+str(c)+'\n')
        f.close()


#=====Cluster Documents=====#
#data = loadData(True, 1, True)

#Bad results
#cluster = KMeans(n_clusters=10, random_state=123).fit(data)

#Not really better
#cluster = DBSCAN(eps=10, min_samples=3).fit(data)

#Also shit
#cluster = DBSCAN(eps=0.8, min_samples=5, algorithm='brute', metric='cosine').fit(data)


#=====Cluster Words=====#
data = loadData(True, 1, False)

#Results seem good
cluster = KMeans(n_clusters=5, random_state=123).fit(data)

#Results look ok
#cluster = DBSCAN(eps=5, min_samples=3).fit(data)

#Funzt nicht wirklich
#cluster = DBSCAN(eps=0.988, min_samples=1, algorithm='brute', metric='cosine').fit(data)

#visualize(data, cluster.labels_)
#outputDistances(data, "cosine")
getSampleWords(cluster.labels_)