"""Makes different n-Gram collections from agisi data and writes some statistics to ngram_result.txt"""
#Preprocessing.py must be run in advance

import numpy as np
import math


def prepareData(path):
    """reads result from preprocessing"""
    words = []
    with open(path) as f:
        definitions=[line for line in f] # list of definitions
        for line in definitions:
            line=line[:-1] # remove \n
            lineWords = []
            for element in line.split(','): # words are seprated by ,
                element = element[1:-1] # remove quotes
                lineWords.append(element)
            words.append(lineWords)
    return words

def makeNGrams(words, n):
    """ makes n_grams out of given string[][] where each line represents one definition"""
    n_grams = []
    for line in words:
        n_grams_line = []
        for i in range(len(line)-n+1): # number of n-grams in line
            term = ""
            for j in range(i, i+n): # n words from starting position 
                term += line[j] + " "
            term = term[:-1] # remove last space
            n_grams_line.append(term)
        n_grams.append(n_grams_line)
    return n_grams

def makeMultiplicityDictionary(n_grmas):
    """Used only for \label{ngramme} table and as helper for dtm creation"""
    result = {}
    for line in n_grams:
        for term in line:
            if term in result:
                result[term] += 1
            else:
                result[term] = 1
    return result

def makeDocoumentTermMatrix(n_grams):
    """Makes a dtm used for tf-idf or lda
    Contains a header line and column containing term and document number
    when clustering use [i[1:] for i in result[1:]]"""
    keys = list(makeMultiplicityDictionary(n_grams).keys())
    dtm = [[0 for x in range(len(n_grams)+1)] for y in range(len(keys)+1)]

    for i, word in enumerate(keys): #set line headers
        dtm[i+1][0] = word
        for j, line in enumerate(n_grams):
            dtm[0][j+1] = str(j) #set column header
            for term in line:
                if term == word:
                    dtm[i+1][j+1]+=1

    return dtm

def exportDictionary(dictionary, path, sampleCount):
    """Wrties dictionary to human readble to file"""
    keys = list(dictionary.keys())
    values = list(dictionary.values())
    combined = list(zip(values, keys))
    combined = sorted(combined)
    f = open(path, 'w+')
    header = ': Number of different terms: ' + str(len(combined)) + '\nmost common terms:\n'
    f.write(header)
    for k in range (1,sampleCount):
        sample = '' + combined[-k][1]+'('+str(combined[-k][0])+')\n'
        f.write(sample)

def exportDtm(dtm, path):
    """Writes dtm to .npy binary file"""
    np.save(path, dtm)


words = prepareData('agisi_data_preprocessed.csv')
for n in range(1,3+1): # which n's should be produced
    n_grams = makeNGrams(words, n)

    dictionary = makeMultiplicityDictionary(n_grams)
    exportDictionary(dictionary, 'n_grams_dict_'+str(n)+'.txt', 20)

    dtm = makeDocoumentTermMatrix(n_grams)
    exportDtm(dtm, 'dtm_'+str(n))