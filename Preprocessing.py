import re
import csv
from nltk.corpus import stopwords
from nltk import stem
from nltk import tokenize


# Load data from file, save source of each definition in agisi_sources array
agisi_definitions = []
agisi_sources = []
with open('agisi_data_piped.csv') as csv_file:
    reader = csv.DictReader(csv_file, delimiter='|')
    for row in reader:
        agisi_definitions.append(row['definition'])
        if re.search(r'AGISI',row['id']):
            if re.search(r'MI',row['id']):
                agisi_sources.append('mi')
            elif re.search(r'HI', row['id']):
                agisi_sources.append('hi')
            else:
                agisi_sources.append('i')
        else:
            agisi_sources.append('lh')


# Remove stop word tokens by using NLTKs English stop word library
def removeStopwords(stringset):
    stop_words = set(stopwords.words("english"))
    filtered_set = []
    for word in stringset:
        if word.lower() not in stop_words:
            filtered_set.append(word)
    return filtered_set


# Split up definitions into tokens by using NLTKs regular expression tokenizer with a custom regex statement
regex_statement = r'[a-zA-Z\u00C0-\u00FF]+'
tokenizer = tokenize.RegexpTokenizer(regex_statement)

agisi_unfiltered_tokens = []
for definition in agisi_definitions:
    agisi_unfiltered_tokens.append(tokenizer.tokenize(definition))


# Remove stop word tokens
agisi_filtered_tokens = []
for definition in agisi_unfiltered_tokens:
    agisi_filtered_tokens.append(removeStopwords(definition))


# Stem the remaining words
stemmer = stem.snowball.EnglishStemmer()
agisi_stemmed_tokens = []
for definition in agisi_filtered_tokens:
    stemmed_tokens = []
    for token in definition:
        stemmed_tokens.append(stemmer.stem(token))
    agisi_stemmed_tokens.append(stemmed_tokens)


# Merge common multi word expressions into one token
dictionary = [
    ('e', 'g'),                 # 20 occurencies
    ('machin', 'intellig'),     # 176 occurencies
    ('human', 'intellig'),      # 120 occurencies
    ('artifici', 'intellig')    # 48 occurencies
]

tokenizer = tokenize.MWETokenizer(dictionary, separator='_')
agisi_mwe_tokens = []
for definition in agisi_stemmed_tokens:
    agisi_mwe_tokens.append(tokenizer.tokenize(definition))


# Compute and print quantity of all remaining terms (for comparison reasons)
token_count = 0
for definition in agisi_mwe_tokens:
    if len(definition) > 0:
        for token in definition:
            token_count+=1
print("Number of all tokens: ", token_count)


# Write source of each definition to the first position of every term array
for i in range(0, len(agisi_mwe_tokens)):
    agisi_mwe_tokens[i].insert(0, agisi_sources[i])


# Save preprocessed data to .csv file, using ',' as delimiter between 2 tokens and quoting each token with '
with open('agisi_data_preprocessed.csv', mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',', quotechar="\'", quoting=csv.QUOTE_ALL)
    for definition in agisi_mwe_tokens:
        csv_writer.writerow(definition)

print("Wrote dataset to ", csv_file.name)