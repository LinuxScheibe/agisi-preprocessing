#Import all the dependencies
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize

import csv
import re
from nltk import stem
from nltk import tokenize
from nltk.corpus import stopwords

# Remove stop word tokens by using NLTKs English stop word library
def removeStopwords(stringset):
    stop_words = set(stopwords.words("english"))
    filtered_set = []
    for word in stringset:
        if word.lower() not in stop_words:
            filtered_set.append(word)
    return filtered_set


# Load data from file
agisi_definitions = []
agisi_sources = []
with open('agisi_data_piped.csv') as csv_file:
    reader = csv.DictReader(csv_file, delimiter='|')
    for row in reader:
        agisi_definitions.append(row['definition'])
        if re.search(r'AGISI',row['id']):
            if re.search(r'MI',row['id']):
                agisi_sources.append('mi')
            elif re.search(r'HI', row['id']):
                agisi_sources.append('hi')
            else:
                # Only AGISI found
                agisi_sources.append('i')
        else:
            # AGISI not found
            agisi_sources.append('lh')

# Split up definitions into tokens by using NLTKs regular expression tokenizer with a custom regex statement
regex_statement = '[a-zA-Záéíóú]+'
tokenizer = tokenize.RegexpTokenizer(regex_statement)

agisi_unfiltered_tokens = []
for definition in agisi_definitions:
    agisi_unfiltered_tokens.append(tokenizer.tokenize(definition))

print("Regex: ", regex_statement)
#print("Unfiltered tokens: \n", len(agisi_unfiltered_tokens), "\n", agisi_unfiltered_tokens)

# Remove stop word tokens
agisi_filtered_tokens = []
for definition in agisi_unfiltered_tokens:
    agisi_filtered_tokens.append(removeStopwords(definition))
#print("Filtered tokens: \n", [agisi_filtered_tokens[i] for i in (1, 100, 200, 300, 400)])

# Stem the remaining words
stemmer = stem.snowball.EnglishStemmer()
agisi_stemmed_tokens = []
for definition in agisi_filtered_tokens:
    stemmed_tokens = []
    for token in definition:
        stemmed_tokens.append(stemmer.stem(token))
    agisi_stemmed_tokens.append(stemmed_tokens)

#print("Stemmed tokens: \n", agisi_stemmed_tokens)

# Merge common multi word expressions into one token
dictionary = [
    ('e', 'g'),                 # 20 occurencies
    ('machin', 'intellig'),     # 176 occurencies
    ('human', 'intellig'),      # 120 occurencies
    ('artifici', 'intellig')    # 48 occurencies
]
tokenizer = tokenize.MWETokenizer(dictionary, separator=' ')
agisi_mwe_tokens = []
for definition in agisi_stemmed_tokens:
    agisi_mwe_tokens.append(tokenizer.tokenize(definition))

token_count = 0
for definition in agisi_mwe_tokens:
    if len(definition)>0:
        for token in definition:
            token_count+=1
print("Number of all tokens: ", token_count)

for i in range(0, len(agisi_mwe_tokens)):
    agisi_mwe_tokens[i].insert(0, agisi_sources[i])

# Save preprocessed data to .csv file, using ',' as delimiter between 2 tokens and quoting each token with '
with open('agisi_data_preprocessed.csv', mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',', quotechar="\'", quoting=csv.QUOTE_ALL)
    for definition in agisi_mwe_tokens:
        csv_writer.writerow(definition)

print("Wrote dataset to ", csv_file.name)

#Recreate sentences with preprocessed data
agisi_stemmed_sentences_all = []
id = []
i = 1
for definition in agisi_mwe_tokens:
    tmp = ""
    definition.pop(0)
    for word in definition:
        tmp += str(word)+" "
        id.append(i)
        i += 1
    agisi_stemmed_sentences_all.append(tmp)

#give every document an id
tagged_data = [TaggedDocument(words=word_tokenize(_d.lower()), tags=[str(i)]) for i, _d in enumerate(agisi_stemmed_sentences_all)]

#train the doc2vec model
max_epochs = 25000
vec_size = 2
alpha = 0.025

model = Doc2Vec(size=vec_size,
                alpha=alpha, 
                min_alpha=0.00025,
                min_count=1,
                #dm =1) #‘distributed memory’ (PV-DM)
                dm =0) #‘distributed bag of words’ (PV-DBOW)
  
model.build_vocab(tagged_data)

for epoch in range(max_epochs):
    print('iteration {0}'.format(epoch))
    model.train(tagged_data,
                total_examples=model.corpus_count,
                epochs=model.iter)
    # decrease the learning rate
    model.alpha -= 0.0002
    # fix the learning rate, no decay
    model.min_alpha = model.alpha

model.save("d2v.model")
print("Model Saved")

from gensim.models.doc2vec import Doc2Vec

#get the definition names from the list
definitions = []
with open("definition_names.txt") as file:
    definitions = file.read().splitlines()

#get the vectors out of the model, connect it with the definition names and safe the data
vectors = []
for i in range(len(agisi_stemmed_sentences_all)):
    vector = model.docvecs[str(i)]
    vec_tmp = definitions[i] + "," + str(vector[0]) + "," + str(vector[1])
    vectors.append(vec_tmp)

with open("doc2vec_dbow_2d.txt", 'w') as file:
    for vector in vectors:
        file.write(vector)
        file.write('\n')