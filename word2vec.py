import re
import csv
from nltk.corpus import stopwords  
from nltk import stem
from nltk import tokenize
import pandas as pd
import tensorflow as tf
import numpy as np
#import time
import matplotlib.pyplot as plt
import json

# Load data from file
agisi_definitions = []
agisi_sources = []
with open('agisi_data_piped.csv') as csv_file:
    reader = csv.DictReader(csv_file, delimiter='|')
    for row in reader:
        agisi_definitions.append(row['definition'])
        if re.search(r'AGISI',row['id']):
            if re.search(r'MI',row['id']):
                agisi_sources.append('mi')
            elif re.search(r'HI', row['id']):
                agisi_sources.append('hi')
            else:
                # Only AGISI found
                agisi_sources.append('i')
        else:
            # AGISI not found
            agisi_sources.append('lh')

# Remove stop word tokens by using NLTKs English stop word library
def removeStopwords(stringset):
    stop_words = set(stopwords.words("english"))
    filtered_set = []
    for word in stringset:
        if word.lower() not in stop_words:
            filtered_set.append(word)
    return filtered_set

# Split up definitions into tokens by using NLTKs regular expression tokenizer with a custom regex statement
regex_statement = '[a-zA-Záéíóú]+'
tokenizer = tokenize.RegexpTokenizer(regex_statement)

agisi_unfiltered_tokens = []
for definition in agisi_definitions:
    agisi_unfiltered_tokens.append(tokenizer.tokenize(definition))

#print("Regex: ", regex_statement)
#print("Unfiltered tokens: \n", len(agisi_unfiltered_tokens), "\n", agisi_unfiltered_tokens)

# Remove stop word tokens
agisi_filtered_tokens = []
for definition in agisi_unfiltered_tokens:
    agisi_filtered_tokens.append(removeStopwords(definition))
#print("Filtered tokens: \n", [agisi_filtered_tokens[i] for i in (71)])

# Stem the remaining words
stemmer = stem.snowball.EnglishStemmer()
agisi_stemmed_tokens = []
for definition in agisi_filtered_tokens:
    stemmed_tokens = []
    for token in definition:
        stemmed_tokens.append(stemmer.stem(token))
    agisi_stemmed_tokens.append(stemmed_tokens)

#print("Stemmed tokens: \n", agisi_stemmed_tokens)

# Merge common multi word expressions into one token
dictionary = [
    ('e', 'g'),                 # 20 occurencies
    ('machin', 'intellig'),     # 176 occurencies
    ('human', 'intellig'),      # 120 occurencies
    ('artifici', 'intellig')    # 48 occurencies
]

tokenizer = tokenize.MWETokenizer(dictionary, separator='_')
agisi_mwe_tokens = []
for definition in agisi_stemmed_tokens:
    agisi_mwe_tokens.append(tokenizer.tokenize(definition))


token_count = 0
for definition in agisi_mwe_tokens:
    if len(definition)>0:
        for token in definition:
            token_count+=1
print("Number of all tokens: ", token_count)


for i in range(0, len(agisi_mwe_tokens)):
    agisi_mwe_tokens[i].insert(0, agisi_sources[i])

# Save preprocessed data to .csv file, using ',' as delimiter between 2 tokens and quoting each token with '
with open('agisi_data_preprocessed.csv', mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',', quotechar="\'", quoting=csv.QUOTE_ALL)
    for definition in agisi_mwe_tokens:
        print(definition)
        csv_writer.writerow(definition)

print("Wrote dataset to ", csv_file.name)

#recreate the sentences with the preprocessed words
words_mi = []
words_hi = []
words_i = []
words_lh = []
words_all = []
id = []
i = 1
agisi_stemmed_sentences_mi = []
agisi_stemmed_sentences_hi = []
agisi_stemmed_sentences_i = []
agisi_stemmed_sentences_lh = []
agisi_stemmed_sentences_all = []
for definition in agisi_mwe_tokens:
    tmp = ""
    category = definition[0]
    print(category)
    definition.pop(0)

    if(category == "mi"):
        for word in definition:
            tmp += str(word)+" "
            words_mi.append(word)
            id.append(i)
            i += 1
        print(tmp)
        agisi_stemmed_sentences_mi.append(tmp)
    if(category == "hi"):
        for word in definition:
            tmp += str(word)+" "
            words_hi.append(word)
            id.append(i)
            i += 1
        print(tmp)
        agisi_stemmed_sentences_hi.append(tmp)
    if(category == "i"):
        for word in definition:
            tmp += str(word)+" "
            words_i.append(word)
            id.append(i)
            i += 1
        print(tmp)
        agisi_stemmed_sentences_i.append(tmp)
    if(category == "lh"):
        for word in definition:
            tmp += str(word)+" "
            words_lh.append(word)
            id.append(i)
            i += 1
        print(tmp)
        agisi_stemmed_sentences_lh.append(tmp)
        
    for word in definition:
        tmp += str(word)+" "
        words_all.append(word)
        id.append(i)
        i += 1
    print(tmp)
    agisi_stemmed_sentences_all.append(tmp)

print("all preprocessed sentences")
print(agisi_stemmed_sentences_all)
print()

words_mi = set(words_mi)
words_hi = set(words_hi)
words_i = set(words_i)
words_lh = set(words_lh)
words_all = set(words_all)
id = set(id)

print("words")
print(words_all)
print()

def saveData(category, dimensions, vectors, countVectors, connections, countConnections, word2int):
    #save all data
    path = "vectors_"+category+"_"+str(dimensions)+"d.csv"
    with open(path, mode='w', newline='') as csv_file:
        for x in range(countVectors):
            tmp_str = ""
            if(dimensions == 2):
                tmp_str += vectors.iat[x,0] + "," + str(vectors.iat[x,1]) + "," + str(vectors.iat[x,2])
            if(dimensions == 3):
                tmp_str += vectors.iat[x,0] + "," + str(vectors.iat[x,1]) + "," + str(vectors.iat[x,2]) + "," + str(vectors.iat[x,3])
            csv_file.write(tmp_str)
            csv_file.write('\n')
            
    print("Saved")
    
    path = "verbindungen_"+category+"_"+str(dimensions)+"d.txt"
    with open(path,'w') as file:
        for x in range(countConnections):
            tmp_str = ""
            tmp_str += connections.iat[x,0] + "," + connections.iat[x,1]
            file.write(tmp_str)
            file.write('\n')
            
    print("Saved")

    path = "word2int_"+category+"_"+str(dimensions)+"d.txt"
    with open(path,'w') as file:
        file.write(json.dumps(word2int))
                
    print("Saved")
    
def createVectors(agisi_sentences, dimensions, WINDOW_SIZE, category, words):
    word2int = {}

    for i,word in enumerate(words):
        word2int[word] = i
    
    sentences = []
    for sentence in agisi_sentences:
        sentences.append(sentence.split())
        
    data = []
    for sentence in sentences:
        for idx, word in enumerate(sentence):
            for neighbor in sentence[max(idx - WINDOW_SIZE, 0) : min(idx + WINDOW_SIZE, len(sentence)) + 1] : 
                if neighbor != word:
                    data.append([word, neighbor])
    
    df = pd.DataFrame(data, columns = ['input', 'label'])
    
    print("data frame head")
    print(df.head(30000))
    print()
    print("data frame shape")
    print(df.shape)
    print()
    print("word2int")
    print(word2int)
    print()
    
    #start the training for the neuronal network
    ONE_HOT_DIM = len(words)

    # function to convert numbers to one hot vectors
    def to_one_hot_encoding(data_point_index):
        one_hot_encoding = np.zeros(ONE_HOT_DIM)
        one_hot_encoding[data_point_index] = 1
        return one_hot_encoding
    
    X = [] # input word
    Y = [] # target word
    
    for x, y in zip(df['input'], df['label']):
        X.append(to_one_hot_encoding(word2int[ x ]))
        Y.append(to_one_hot_encoding(word2int[ y ]))
    
    # convert them to numpy arrays
    X_train = np.asarray(X)
    Y_train = np.asarray(Y)
    
    # making placeholders for X_train and Y_train
    x = tf.placeholder(tf.float32, shape=(None, ONE_HOT_DIM))
    y_label = tf.placeholder(tf.float32, shape=(None, ONE_HOT_DIM))
    
    # word embedding will be 2 dimension for 2d visualization
    EMBEDDING_DIM =  dimensions
    
    # hidden layer: which represents word vector eventually
    W1 = tf.Variable(tf.random_normal([ONE_HOT_DIM, EMBEDDING_DIM]))
    b1 = tf.Variable(tf.random_normal([1])) #bias
    hidden_layer = tf.add(tf.matmul(x,W1), b1)
    
    # output layer
    W2 = tf.Variable(tf.random_normal([EMBEDDING_DIM, ONE_HOT_DIM]))
    b2 = tf.Variable(tf.random_normal([1]))
    prediction = tf.nn.softmax(tf.add( tf.matmul(hidden_layer, W2), b2))
    
    # loss function: cross entropy
    loss = tf.reduce_mean(-tf.reduce_sum(y_label * tf.log(prediction), axis=[1]))
    print(loss)
    
    # training operation
    train_op = tf.train.GradientDescentOptimizer(0.05).minimize(loss)
    
    import time
    start = time.time()
    
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init) 
    
    iteration = 25000
    for i in range(iteration):
        # input is X_train which is one hot encoded word
        # label is Y_train which is one hot encoded neighbor word
        sess.run(train_op, feed_dict={x: X_train, y_label: Y_train})
        if i % 100 == 0:
            percent = (i/iteration)*100
            print(category + ", " + str (dimensions) + ": " + str(percent) + "%")
            
    end = time.time()
    time = (end - start)/60
    print("Done. It took " + str(time) + " minutes.")
    print()
    
    # Now the hidden layer (W1 + b1) is actually the word look up table
    vectors = sess.run(W1 + b1)
    print("vectors")
    print(vectors)
    print()
    
    if(dimensions == 2):
        w2v_df = pd.DataFrame(vectors, columns = ['x1', 'x2'])
        w2v_df['word'] = words
        w2v_df = w2v_df[[ 'word', 'x1', 'x2']]
        w2v_df
    if(dimensions == 3):
        w2v_df = pd.DataFrame(vectors, columns = ['x1', 'x2', 'x3'])
        w2v_df['word'] = words
        w2v_df = w2v_df[[ 'word', 'x1', 'x2', 'x3']]
        w2v_df
    
    fig, ax = plt.subplots()

    if(dimensions == 2):
        for word, x1, x2 in zip(w2v_df['word'], w2v_df['x1'], w2v_df['x2']):
            ax.annotate(word, (x1,x2 ))
        
        PADDING = 1.0
        x_axis_min = np.amin(vectors, axis=0)[0] - PADDING
        y_axis_min = np.amin(vectors, axis=0)[1] - PADDING
        x_axis_max = np.amax(vectors, axis=0)[0] + PADDING
        y_axis_max = np.amax(vectors, axis=0)[1] + PADDING
         
        plt.xlim(x_axis_min,x_axis_max)
        plt.ylim(y_axis_min,y_axis_max)
        plt.rcParams["figure.figsize"] = (20,20)
        
        plt.show()
   
    saveData(category, dimensions, w2v_df, len(w2v_df.index), df, len(df.index), word2int)

#createVectors(agisi_sentences, dimensions, WINDOW_SIZE, category)
createVectors(agisi_stemmed_sentences_mi, 2, 2, "mi", words_mi)
createVectors(agisi_stemmed_sentences_mi, 3, 2, "mi", words_mi)
createVectors(agisi_stemmed_sentences_hi, 2, 2, "hi", words_hi)
createVectors(agisi_stemmed_sentences_hi, 3, 2, "hi", words_hi)
createVectors(agisi_stemmed_sentences_i, 2, 2, "i", words_i)
createVectors(agisi_stemmed_sentences_i, 3, 2, "i", words_i)
createVectors(agisi_stemmed_sentences_lh, 2, 2, "lh", words_lh)
createVectors(agisi_stemmed_sentences_lh, 3, 2, "lh", words_lh)
createVectors(agisi_stemmed_sentences_all, 2, 2, "all", words_all)
createVectors(agisi_stemmed_sentences_all, 3, 2, "all", words_all)
